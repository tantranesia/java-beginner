//package org.example.FirstLesson;
//
//public class Homework {
//    public static void main(String[] args) {
//        int[] numbers = {2,6,9,4,3};
//        int  length = numbers.length;
//
//        for(int i = 0; i <= length; i++) {
//            if(numbers[i] == 4) {
//                break;
//            }
//            System.out.println("Output" + numbers[i]);
//        }
//
////        length value adalah 5, kemudian <= berarti less than atau equals to.
////        program yang dibuat adalah jika i = 0 dan i kurang dari 5 yang merupakan value dari length
////        sedangkan array index-ing selalu dimulai dai 0 yang berarti 0,1,2,3,4 yang mengakibatkan program akan looping index 5 yang tidak ada
////        jika menggunakan <= akan lebih tepat untuk menggunakan length-1 yang berarti index terakhir dari array
////
//    }
//     public static void taskOne () {
//        String buah = "anggur";
//         switch (buah) {
//             case "mangga":
//                 System.out.println("kuning");
//                 break;
//             case "jeruk":
//                     System.out.println("orang");
//                     break;
//             case "anggur":
//                         System.out.println("ungu");
//                         break;
//             default:
//             break;
////             output yang akan keluar adalah ungu karna sesuai dengan variable buah yang memiliki value anggur
////             sehingga ketika kondisinya bertemu maka akan print ungu
//
//         };
//
//         }
//     }
//
//}
