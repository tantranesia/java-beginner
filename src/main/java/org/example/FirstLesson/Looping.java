package org.example.FirstLesson;

public class Looping {
    public static void main(String[] args) {
//       WHILE
        int varCount = 2;
        while (varCount <= 10) {
//            System.out.println("Print this is" + " " + varCount);
            varCount++;
        }
//        DO WHILE
        int number = 15;
        do {
//            System.out.println("Number is" + " " + number);
            number--;
        } while (number >= 0);

//        FOR
        for(int num  = 5; num <= 10; num++) {
//            System.out.println("Var Num: " + num);

        }
        String[] arr = {"Cow", "Camel", "Cat", "Dog", "Bird"};
        int arrLength = arr.length;

        for (int i = 0 ; i < arrLength; i++) {
            String animal = arr[i];
            System.out.println("Animal: " + animal);
        }

//        FOREACH
        for (String string : arr) {
            System.out.println("Print the value: " + string);

        }


    }
}
