package org.example.thirdLesson;

public class YourName {
    private static final String NAME = "Ary";

    public static void main(String[] args) {
        String myName = printMyName(null);
        System.out.println("Output" + myName);
    }

    public static String printMyName(String name) {
        if (name.isEmpty() || name.isBlank()) {
            return NAME;
        } else {
            return name;
        }

    }
}
// The output will be error because name is null the reason is while is does pass the param "name" but on the main it
//null. however, on the conditional if when name isEmpty or isBlank it will return NAME but because NAME is private
//the pritnMyName function cannot access it