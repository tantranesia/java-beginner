package SecondLesson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class JavaLambda {
    public static void main(String[] args) {
        List<String> fruits = Arrays.asList("grape", "apple", "orange");
        List<String> dataFruits = new ArrayList<>();
//java stream
        dataFruits =  fruits.stream().map(String::toUpperCase).toList();

        dataFruits.forEach(System.out::println);

//        java lamba

        dataFruits.forEach(System.out::println);

    }
}
