package SecondLesson;

import java.util.ArrayList;
import java.util.List;

public class ListExample {
    public static void main(String[] args) {
        List<Integer> number = new ArrayList<>();
//        to add value to the array use
        number.add(10);
        number.add(11);

//        to access the value use

        int firstValue = number.get(0);
        int secodnValue = number.get(1);

//        set size
        int length = number.size();

        for(int i = 0; i < length; i++) {
//            System.out.println("FOR LOOP" + number.get(i));
        };

//        for (int testValue : number) {
////            System.out.println();
//        }

//       to change value inside array using .set

        number.set(0, 25);
//        to remove element from index

        number.remove(0);

        for (int testValue : number) {
            System.out.println("check number" + number);
        }




    }
}
