package SecondLesson;

import java.util.HashSet;
import java.util.Set;

public class SetinJava {
    public static void main(String[] args) {
        Set<String> names = new HashSet<>();

//        to add value to Set
        names.add("John");
        names.add("Jane");

//        to get value from Set *notes Set also can return the array unorginased
        for (String customer : names) {
            System.out.println("Customer name: " + customer);
        }

//        delete the index
        names.remove("John");




    }
}
