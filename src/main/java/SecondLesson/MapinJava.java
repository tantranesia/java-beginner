package SecondLesson;

import java.util.HashMap;
import java.util.Map;

public class MapinJava {
    public static void main(String[] args) {
        Map<Integer, String> map = new HashMap<>();
        map.put(1, "One");
        map.put(2, "Two");
        map.put(3, "Three");
        map.put(4, "Four");
        map.put(5, "Five");
        map.put(6, "Six");

//        get the elemetn value based on key
        String firstElement = map.get(1);

//        if(map.containsKey(2)){
//            System.out.println("map contains two");
//        }
//        if(map.containsValue("Six")){
//            System.out.println("map contains six");
//        }

//        how to access element inside a MAP
//        1. KEYSET
//        2. ENTRYSET
//        3. JAVALAMBDA

        for (int key : map.keySet()) {
            System.out.println("key: " + key + " value: " + map.get(key));
        }

//        JAVA LAMBDA
        map.forEach((key, value) -> {
            System.out.println("key: " + key + " value: " + value);
        });
    }
}
